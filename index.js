/*console.log ("Hello World")*/

//Arithmettic Operators

let x = 1397;
let y = 7831;

let sum = x + y;
console.log(sum);
console.log("Result of addition operator: " + sum);

let difference = x - y;
console.log("Result of Subtraction operator: " + difference);

let product = x * y;
console.log("Result of Multiplication operator: " + product);

let quotient = x / y;
console.log("Result of Division operator: " + quotient);

// Modulus (%)
// Gets the remainder from 2 divided values
let remainder = y % x;
console.log("Result of Modulo operator:" + remainder);

// Assignment Operator (=)

// Basic Assignment
let assignmentNumber = 8;

//longhand Method for assignment operator
assignmentNumber = assignmentNumber + 2
console.log("Result of Addition assignment operator:" + assignmentNumber);

// Shorthand Method for assignment operator
assignmentNumber += 2;
console.log("Result of Addition assignment operator:" + assignmentNumber);

assignmentNumber -= 2;
console.log("Result of Subtraction assignment operator:" + assignmentNumber);

assignmentNumber *= 2;
console.log("Result of Multiplication assignment operator:" + assignmentNumber);

assignmentNumber /= 2;
console.log("Result of Division assignment operator:" + assignmentNumber);

// multiple operator and parenthesis

let mdas	= 1 + 2 - 3 * 4 / 5;
console.log("Result of Multiple assignment operator:" + mdas);

let pemdas = 1 + (2 - 3) * (4 / 5);
console.log("Result of pemdas assignment operator:" + pemdas);

pemdas = (1 + ( 2 - 3 )) * (4 / 5);
console.log("Result of 2nd pemdas assignment operator:" + pemdas);

//Incermentation vs Decrementation
//Incrementation (++)
//decrementation (--)

let z = 1;

//++z added 1 to its original value.

let increment = ++z;
console.log("Result of Pre-Incrementation assignment operator:" + increment);
console.log("Result of Pre-Incrementation assignment operator:" + z);

increment = z++;
console.log("Result of Post-Incrementation assignment operator:" + increment);
console.log("Result of Post-Incrementation assignment operator:" + z);


let decrement = --z;
console.log("Result of Pre-decrementation assignment operator:" + decrement);
console.log("Result of Pres-Incrementation assignment operator:" + z);

decrement = z--;
console.log("Result of Post-decrementation-decrementation assignment operator:" + decrement)
console.log("Result of Post-decrementation assignment operator:" + z);

//Type Coercion

let numA = "10";
let numB = 12;

let coercion = numA + numB;
console.log(coercion);
console.log(typeof coercion);

let numC = 16;
let numD = 14;

let nonCoercion = numC + numD;
console.log(nonCoercion);
console.log(typeof nonCoercion);


//false = 0;
let numE = false + 1
console.log(numE);

let numF = true + 1;
console.log(numF);


//Comparison Operators

let juan = "juan";

// Equality Operator (==);
// Checks 2 Operands if they are equal / have the same content.
// May return boolean value

console.log(1 == 1);
console.log(1 == 2);
console.log(1 == "1");
console.log("juan" == "juan");
console.log("juan" == juan);

//Inequality Operator ( != )
// !==  is not

console.log(1 != 1);
console.log(1 != 2);
console.log(1 != "1");
console.log("juan" != "juan");
console.log("juan" != juan);
console.log(0 != false);

// Strict Equality Operator (===)
console.log ( 1===1 );
console.log( 1===2 );
console.log	("juan" === juan);

//Strict Inequaliy (!==)
console.log ( 1!==1 );
console.log( 1!==2 );
console.log	("juan" !== juan);

//Relational Operator
let a = 50;
let b = 65;

// GT (>) Greater than Operator
let isGreaterThan = a > b;
console.log(isGreaterThan);

//LT (<) Less Than Operator
let isLessThan = a < b;
console.log(isLessThan);

// GTE (>=)
let isGTorEqual = a >= b;
console.log(isGTorEqual);

//LTE (<=)
let isLTorEqual = a <= b;
console.log(isLTorEqual);

let numStr = "30";
console.log (a > numStr)

let str = "twenty";
console.log(b >= str);
// In some events, we can receive Nan
//nan == Not a number

//Logical Operators
let isLegalAge = true;
let isRegistered = false;

// Logical AND Operator (&& - Ampersands)
let allRequirementsMet = isLegalAge && isRegistered;
console.log("Result of Logical and Operator: " + allRequirementsMet);

// Logical OR Operator (|| - Double Pipe)
let someRequirementsMet = isLegalAge || isRegistered;
console.log("Result of Logical and Operator: " + someRequirementsMet);

//logical NOT Operator (! - Exclamation Point)
// Return Opposite Value

let someRequirementsNotMet = !isRegistered;
console.log("Results of Logical Not Operator: " +someRequirementsNotMet );


// Data types are arrays and strings, 